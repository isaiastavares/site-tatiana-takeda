import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { VideoService } from '../admin/video/video.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit, OnDestroy {

  videos: any;
  videosSub: any;

  constructor(private titleService: Title, private videoService: VideoService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Vídeos');

    this.videosSub = this.videoService.findAll().subscribe(videos => {
      this.videos = videos;
    });
  }

  ngOnDestroy() {
    this.videosSub.unsubscribe();
  }

}

