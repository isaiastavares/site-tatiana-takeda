import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public menu = 'home';

  constructor(private router: Router) { }

  ngOnInit() {
    console.log(this.router.url);
  }

  home() {
    this.router.navigate(['']);
    this.menu = 'home';
  }

  eventos() {
    this.router.navigate(['eventos']);
    this.menu = 'eventos';
  }

  blog() {
    this.router.navigate(['blog']);
    this.menu = 'blog';
  }

  videos() {
    this.router.navigate(['videos']);
    this.menu = 'videos';
  }

  legislacao() {
    this.router.navigate(['legislacao']);
    this.menu = 'legislacao';
  }

  projetos() {
    this.router.navigate(['projetos']);
    this.menu = 'projetos';
  }

  direitos() {
    this.router.navigate(['direitos']);
    this.menu = 'direitos';
  }

}
