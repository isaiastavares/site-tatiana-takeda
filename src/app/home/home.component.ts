import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { VideoService } from '../admin/video/video.service';
import { HomeService } from './home.service';
import { EventoService } from '../admin/evento/evento.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  eventos: any;
  videos: any;
  comments: any;

  eventosSub: any;
  videosSub: any;
  commentsSub: any;

  // name: string;
  // email: string;
  // text: string;
  commentForm: any;

  constructor(private titleService: Title, private homeService: HomeService, public sanitizer: DomSanitizer,
    private videoService: VideoService, private eventoService: EventoService) {
  }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Início');
    this.clear();

    this.eventosSub = this.eventoService.findByLimit(3).subscribe(eventos => {
      this.eventos = eventos;
    });

    this.videosSub = this.videoService.findByLimit(3).subscribe(videos => {
      this.videos = videos;
    });

    this.commentsSub = this.homeService.findCommentsByLimitTen().subscribe(comments => {
      this.comments = comments;
    });
  }

  add(commentForm) {
    console.log(JSON.stringify(commentForm));
    if (commentForm.name !== '' && commentForm.text !== '') {
      this.homeService.add(commentForm);
      this.clear();
    }
  }

  clear() {
    this.commentForm = {
      name: '',
      email: '',
      text: ''
    };
  }

  ngOnDestroy() {
    this.eventosSub.unsubscribe();
    this.videosSub.unsubscribe();
    this.commentsSub.unsubscribe();
  }

}

