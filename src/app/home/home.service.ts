import { Injectable } from '@angular/core';
import { FirestoreService } from '../core/service/firestore.service';

@Injectable()
export class HomeService {

    constructor(private db: FirestoreService) {
    }

    findCommentsByLimitTen() {
        return this.db.colWithIds$('comments', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(10)
        );
    }

    findVideosByLimitThree() {
        return this.db.colWithIds$('videos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(3)
        );
    }

    findEventosByLimitThree() {
        return this.db.colWithIds$('eventos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(3)
        );
    }

    add(comment) {
        comment.enable = true;
        this.db.add('comments', comment)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

}
