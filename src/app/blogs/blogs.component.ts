import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BlogService } from '../admin/blog/blog.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit, OnDestroy {

  blogs: any;
  blogsSub: any;

  constructor(private titleService: Title, private blogService: BlogService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Blog');

    this.blogsSub = this.blogService.findAll().subscribe(blogs => {
      this.blogs = blogs;
    });
  }

  ngOnDestroy() {
    this.blogsSub.unsubscribe();
  }

}
