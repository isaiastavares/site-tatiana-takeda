import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProjetoService } from '../admin/projeto/projeto.service';

@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.component.html',
  styleUrls: ['./projeto.component.css']
})
export class ProjetoComponent implements OnInit, OnDestroy {

  projetos: any;
  projetosSub: any;

  constructor(private titleService: Title, private projetoService: ProjetoService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Projetos de Lei');

    this.projetosSub = this.projetoService.findAll().subscribe(projetos => {
      this.projetos = projetos;
    });
  }

  ngOnDestroy() {
    this.projetosSub.unsubscribe();
  }

}
