import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

const Instafeed = require('instafeed.js');

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  year: number;

  constructor(private router: Router) { }

  ngOnInit() {
    this.year = new Date().getFullYear();

    const userFeed = new Instafeed({
      get: 'user',
      // userId: '1034967833',
      userId: '8771236996',
      limit: 8,
      resolution: 'thumbnail',
      accessToken: '8771236996.1677ed0.b153a7b1d77541be926f3f91d3ec8120',
      sortBy: 'most-recent',
      template: '<div class="gallery-feed">' +
                  '<a href="{{link}}" title="{{caption}}" target="_blank">' +
                    '<img src="{{image}}" class="img img-raised rounded" alt="{{caption}}">' +
                  '</a>' +
                '</div>'
    });
    userFeed.run();
  }

  facebook() {
    window.open(
      'https://www.facebook.com/Direitos-e-Inclus%C3%A3o-das-Pessoas-com-Defici%C3%AAncia-Prof-Tatiana-Takeda-1697152990570307/'
      , '_blank');
  }

  instagram() {
    window.open('https://www.instagram.com/direitoeinclusao/', '_blank');
  }

  youtube() {
    window.open('https://www.youtube.com/channel/UCoNH0DjhePFZvhrpbZBFlsQ', '_blank');
  }

  home() {
    this.router.navigate(['']);
  }

  eventos() {
    this.router.navigate(['eventos']);
  }

  blog() {
    this.router.navigate(['blog']);
  }

  videos() {
    this.router.navigate(['videos']);
  }

  legislacao() {
    this.router.navigate(['legislacao']);
  }

  projetos() {
    this.router.navigate(['projetos']);
  }

  direitos() {
    this.router.navigate(['direitos']);
  }

}
