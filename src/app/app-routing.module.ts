import { Routes } from '@angular/router';
import { MainComponent } from './admin/main/main.component';
import { BlogsComponent } from './blogs/blogs.component';
import { AuthGuard } from './core/service/auth.guard';
import { DireitosComponent } from './direitos/direitos.component';
import { EventosComponent } from './eventos/eventos.component';
import { HomeComponent } from './home/home.component';
import { LegislacaoComponent } from './legislacao/legislacao.component';
import { LoginComponent } from './login/login.component';
import { ProjetoComponent } from './projeto/projeto.component';
import { SiteComponent } from './site/site.component';
import { VideosComponent } from './videos/videos.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: SiteComponent,
    children: [{
      path: '',
      component: HomeComponent
    },
    {
      path: 'eventos',
      component: EventosComponent
    },
    {
      path: 'blog',
      component: BlogsComponent
    },
    {
      path: 'videos',
      component: VideosComponent
    },
    {
      path: 'legislacao',
      component: LegislacaoComponent
    },
    {
      path: 'projetos',
      component: ProjetoComponent
    },
    {
      path: 'direitos',
      component: DireitosComponent
    }],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [{
      path: 'evento',
      loadChildren: './admin/evento/evento.module#EventoModule'
    },
    {
      path: 'blog',
      loadChildren: './admin/blog/blog.module#BlogModule'
    },
    {
      path: 'video',
      loadChildren: './admin/video/video.module#VideoModule'
    },
    {
      path: 'legislacao',
      loadChildren: './admin/legislacao/legislacao.module#LegislacaoModule'
    },
    {
      path: 'projetos',
      loadChildren: './admin/projeto/projeto.module#ProjetoModule'
    },
    {
      path: 'direitos',
      loadChildren: './admin/direito/direito.module#DireitoModule'
    }]
  }];
