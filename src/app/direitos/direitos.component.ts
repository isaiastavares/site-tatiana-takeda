import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { EventoService } from '../admin/evento/evento.service';
import { DireitoService } from '../admin/direito/direito.service';

@Component({
  selector: 'app-direitos',
  templateUrl: './direitos.component.html',
  styleUrls: ['./direitos.component.css']
})
export class DireitosComponent implements OnInit, OnDestroy {

  direitos: any;
  direitosSub: any;

  constructor(private titleService: Title, private direitoService: DireitoService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Como buscar seus direitos');

    this.direitosSub = this.direitoService.findAll().subscribe(direitos => {
      this.direitos = direitos;
    });
  }

  ngOnDestroy() {
    this.direitosSub.unsubscribe();
  }

}
