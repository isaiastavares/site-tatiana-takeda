export interface UserFirestore {
    id: string;
    displayName: string;
    photoURL: string;
    email: string;
    enable: boolean;
}
