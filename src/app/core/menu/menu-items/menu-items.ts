import { Injectable } from '@angular/core';

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'evento',
    name: 'Eventos',
    type: 'link',
    icon: 'icon-speedometer icons',
  },
  {
    state: 'blog',
    name: 'Blog',
    type: 'link',
    icon: 'icon-speedometer icons',
  },
  {
    state: 'video',
    name: 'Vídeos',
    type: 'link',
    icon: 'icon-speedometer icons',
  },
  {
    state: 'legislacao',
    name: 'Legislação',
    type: 'link',
    icon: 'icon-speedometer icons',
  },
  {
    state: 'projetos',
    name: 'Projetos de Lei',
    type: 'link',
    icon: 'icon-speedometer icons',
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
