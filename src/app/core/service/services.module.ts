import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirestoreService } from './firestore.service';
import { AuthService } from './auth.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        FirestoreService,
        AuthService
    ]
})
export class ServicesModule { }
