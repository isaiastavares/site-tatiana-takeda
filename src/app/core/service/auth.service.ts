import { Injectable, Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

    defaultAvatar = 'https://freeiconshop.com/wp-content/uploads/edd/person-outline-filled.png';

    isLoggedIn = false;
    user;

    // store the URL so we can redirect after logging in
    redirectUrl: string;

    constructor(private afAuth: AngularFireAuth, private router: Router) {
        this.afAuth.authState.subscribe(auth => {
            if (auth) {
                this.user = auth;
                this.isLoggedIn = true;
                if (this.redirectUrl) {
                    console.log('Redirecionando para ' + this.redirectUrl);
                    this.router.navigate([this.redirectUrl]);
                } else {
                    // Redirect logged in user here
                    console.log('Redirecionando para videos');
                    this.router.navigate(['admin/videos']);
                }
            } else {
                // Redirect non-logged in user back to home
                console.log('Redirecionando para login');
                this.router.navigate(['/login']);
            }
        });
    }

    // get current user data from firebase
    getUserData() {
        return this.afAuth.auth.currentUser;
    }

    login(email: string, password: string) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password).then(userCredential => {
            // if user info is empty
            if (!userCredential.user.displayName || !userCredential.user.photoURL) {
                this.updateUserProfile(userCredential.user);
            }
            console.log('Logando na aplicação');
            this.router.navigate(['/admin/video']);
        });
    }

    logout() {
        this.afAuth.auth.signOut().then(() => {
            console.log('Fazendo logout');
            this.router.navigate(['/login']);
        });
    }

    // update user display name and photo
    updateUserProfile(user) {
        this.getUserData().updateProfile({
            displayName: user.displayName ? user.displayName : user.email,
            photoURL: user.photoURL ? user.photoURL : this.defaultAvatar
        });
    }
}
