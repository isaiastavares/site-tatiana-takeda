import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { FirebaseConfig } from 'src/environments/firebase.config';
import { BlogService } from './admin/blog/blog.service';
import { EventoService } from './admin/evento/evento.service';
import { LegislacaoService } from './admin/legislacao/legislacao.service';
import { MainComponent } from './admin/main/main.component';
import { ProjetoService } from './admin/projeto/projeto.service';
import { VideoService } from './admin/video/video.service';
import { AppRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsComponent } from './blogs/blogs.component';
import { MenuItems } from './core/menu/menu-items/menu-items';
import { MenuToggleModule } from './core/menu/menu-toggle.module';
import { PageTitleService } from './core/page-title/page-title.service';
import { AuthGuard } from './core/service/auth.guard';
import { ServicesModule } from './core/service/services.module';
import { Error404Component } from './error404/error404.component';
import { EventosComponent } from './eventos/eventos.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { HomeService } from './home/home.service';
import { LegislacaoComponent } from './legislacao/legislacao.component';
import { LoginComponent } from './login/login.component';
import { SafePipe } from './pipe/pipe.component';
import { ProjetoComponent } from './projeto/projeto.component';
import { SiteComponent } from './site/site.component';
import { VideosComponent } from './videos/videos.component';
import { DireitoService } from './admin/direito/direito.service';
import { DireitosComponent } from './direitos/direitos.component';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent,
    SiteComponent,
    MainComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    EventosComponent,
    BlogsComponent,
    VideosComponent,
    LegislacaoComponent,
    ProjetoComponent,
    DireitosComponent,
    Error404Component,
    SafePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes),
    AngularFireModule.initializeApp(FirebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AngularFireStorageModule,
    MenuToggleModule,
    FlexLayoutModule,
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
    BootstrapModalModule,
    ServicesModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    AuthGuard,
    MenuItems,
    PageTitleService,
    HomeService,
    VideoService,
    EventoService,
    BlogService,
    LegislacaoService,
    ProjetoService,
    DireitoService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
