import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LegislacaoService } from '../admin/legislacao/legislacao.service';

@Component({
  selector: 'app-legislacao',
  templateUrl: './legislacao.component.html',
  styleUrls: ['./legislacao.component.css']
})
export class LegislacaoComponent implements OnInit {

  leisConstituicao: any;
  leisEmendas: any;
  leisFederais: any;
  leisDecretos: any;

  constructor(private titleService: Title, private legislacaoService: LegislacaoService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Legislação');

    this.legislacaoService.findConstituição().subscribe(leisConstituicao => {
      this.leisConstituicao = leisConstituicao;
    });

    this.legislacaoService.findEmendas().subscribe(leisEmendas => {
      this.leisEmendas = leisEmendas;
    });

    this.legislacaoService.findLeisFederais().subscribe(leisFederais => {
      this.leisFederais = leisFederais;
    });

    this.legislacaoService.findDecretos().subscribe(leisDecretos => {
      this.leisDecretos = leisDecretos;
    });
  }

}
