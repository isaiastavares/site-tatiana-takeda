import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';

@Injectable()
export class ProjetoService {

    constructor(private db: FirestoreService) {
    }

    findAll() {
        return this.db.colWithIds$('projetos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
        );
    }

    findByLimit(limit: number) {
        return this.db.colWithIds$('projetos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(limit)
        );
    }

    findById(id) {
        return this.db.docWithIds$('projetos/' + id);
    }

    add(projeto) {
        projeto.enable = true;
        this.db.add('projetos', projeto)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(projeto) {
        this.db.update('projetos/' + projeto.id, projeto)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(projeto) {
        projeto.enable = false;
        this.edit(projeto);
    }

}
