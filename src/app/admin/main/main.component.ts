import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { NavigationEnd, Router } from '@angular/router';
import PerfectScrollbar from 'perfect-scrollbar';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/service/auth.service';
import { MenuItems } from 'src/app/core/menu/menu-items/menu-items';
import { PageTitleService } from 'src/app/core/page-title/page-title.service';
declare var $: any;

const screenfull = require('screenfull');

@Component({
    selector: 'app-layout',
    templateUrl: './main-material.html',
    styleUrls: ['./main-material.css'],
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit, OnDestroy {

    private _router: Subscription;
    header: string;
    currentLang = 'en';
    url: string;
    showSettings = false;
    themeSkinColor: any = 'light';
    dark: boolean;
    boxed: boolean;
    collapseSidebar: boolean;
    compactSidebar: boolean;
    customizerIn = false;
    sidebarClosed = false;
    root = 'ltr';
    chatpanelOpen = false;

    private _mediaSubscription: Subscription;
    sidenavOpen = true;
    sidenavMode = 'side';
    isMobile = false;
    public innerWidth: any;

    @ViewChild('sidenav') sidenav;

    _opened = true;
    _mode = 'push';
    _closeOnClickOutside = false;
    _showBackdrop = false;
    private _routerEventsSubscription: Subscription;

    isFullscreen = false;

    subscribe: any;
    user: any;

    @ViewChild('templateRef') public templateRef: TemplateRef<any>;

    public _toggleOpened(): void {
        this._opened = !this._opened;
    }

    constructor(public menuItems: MenuItems, private router: Router, private pageTitleService: PageTitleService,
        private media: ObservableMedia, private authService: AuthService) {

        // this.user = this.userService.getBlankRecord();

        this.user = this.authService.getUserData();

        // this.userService.getCurrentUser().subscribe(user => {
        //     this.user = user;
        // });
    }

    ngOnInit() {
        this.innerWidth = window.innerWidth;
        this.pageTitleService.title.subscribe((val: string) => {
            this.header = val;
        });

        // this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
        //     this.url = event.url;
        // });

        this._router = this.router.events.subscribe((event: NavigationEnd) => {
            this.url = event.url;
        });

        if (this.url !== '/login') {

            const elemSidebar = <HTMLElement>document.querySelector('.sidebar-container');

            /** Perfect scrollbar for sidebar menu **/
            if (window.matchMedia(`(min-width: 960px)`).matches) {
                const ps = new PerfectScrollbar(elemSidebar, {
                    wheelSpeed: 2,
                    wheelPropagation: true,
                    minScrollbarLength: 20
                });
                ps.update();
            }
        }

        if (this.media.isActive('xs') || this.media.isActive('sm')) {
            this._mode = 'over';
            this._closeOnClickOutside = true;
            this._showBackdrop = true;
            this._opened = false;
            this.sidebarClosed = false;
        }

        this._mediaSubscription = this.media.asObservable().subscribe((change: MediaChange) => {
            const isMobile = (change.mqAlias === 'xs') || (change.mqAlias === 'sm');

            this.isMobile = isMobile;
            this._mode = (isMobile) ? 'over' : 'push';
            this._closeOnClickOutside = (isMobile) ? true : false;
            this._showBackdrop = (isMobile) ? true : false;
            this._opened = !isMobile;
            this.sidebarClosed = false;
        });

        this._routerEventsSubscription = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd && this.isMobile) {
                this.sidenav.close();
            }
        });

        // Add slideDown animation to dropdown
        $('.dropdown').on('show.bs.dropdown', function (e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown(500);
        });

        // Add slideUp animation to dropdown
        $('.dropdown').on('hide.bs.dropdown', function (e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp(500);
        });
    }

    ngOnDestroy() {
        this._router.unsubscribe();
        this._mediaSubscription.unsubscribe();
        // this.subscribe.unsubscribe();
    }

    menuMouseOver(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && this.collapseSidebar) {
            this._mode = 'over';
        }
    }

    menuMouseOut(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && this.collapseSidebar) {
            this._mode = 'push';
        }
    }

    toggleFullscreen() {
        if (screenfull.enabled) {
            screenfull.toggle();
            this.isFullscreen = !this.isFullscreen;
        }
    }

    sidebarClosedFunction() {
        this.sidebarClosed = !this.sidebarClosed;
    }

    changeThemeColor(color) {
        this.themeSkinColor = color;
    }

    onActivate(e, scrollContainer) {
        scrollContainer.scrollTop = 0;
    }

    logout() {
        return this.authService.logout();
    }

}
