import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DireitoService } from './direito.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // RouterModule.forChild(VideoRoutes)
    ],
    declarations: [
        // VideoListComponent,
        // VideoFormComponent
    ],
    providers: [
        DireitoService
    ]
})

export class DireitoModule { }
