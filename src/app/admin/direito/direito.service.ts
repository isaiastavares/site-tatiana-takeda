import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';

@Injectable()
export class DireitoService {

    constructor(private db: FirestoreService) {
    }

    findAll() {
        return this.db.colWithIds$('direitos', ref => ref
            .where('enable', '==', true)
            .orderBy('title', 'asc')
        );
    }

    findByLimit(limit: number) {
        return this.db.colWithIds$('direitos', ref => ref
            .where('enable', '==', true)
            .orderBy('title', 'asc')
            .limit(limit)
        );
    }

    findById(id) {
        return this.db.docWithIds$('direitos/' + id);
    }

    add(direito) {
        direito.enable = true;
        this.db.add('direitos', direito)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(direito) {
        this.db.update('direitos/' + direito.id, direito)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(direito) {
        direito.enable = false;
        this.edit(direito);
    }

}
