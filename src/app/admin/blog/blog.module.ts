import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogService } from './blog.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // RouterModule.forChild(VideoRoutes)
    ],
    declarations: [
        // VideoListComponent,
        // VideoFormComponent
    ],
    providers: [
        BlogService
    ]
})

export class BlogModule { }
