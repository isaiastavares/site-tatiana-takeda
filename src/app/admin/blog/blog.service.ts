import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';

@Injectable()
export class BlogService {

    constructor(private db: FirestoreService) {
    }

    findAll() {
        return this.db.colWithIds$('blogs', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
        );
    }

    findByLimit(limit: number) {
        return this.db.colWithIds$('blogs', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(limit)
        );
    }

    findById(id) {
        return this.db.docWithIds$('blogs/' + id);
    }

    add(blog) {
        blog.enable = true;
        this.db.add('blogs', blog)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(blog) {
        this.db.update('blogs/' + blog.id, blog)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(blog) {
        blog.enable = false;
        this.edit(blog);
    }

}
