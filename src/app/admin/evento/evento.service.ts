import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';

@Injectable()
export class EventoService {

    constructor(private db: FirestoreService) {
    }

    findAll() {
        return this.db.colWithIds$('eventos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
        );
    }

    findByLimit(limit: number) {
        return this.db.colWithIds$('eventos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(limit)
        );
    }

    findById(id) {
        return this.db.docWithIds$('eventos/' + id);
    }

    add(evento) {
        evento.enable = true;
        this.db.add('eventos', evento)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(evento) {
        this.db.update('eventos/' + evento.id, evento)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(evento) {
        evento.enable = false;
        this.edit(evento);
    }

}
