import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventoService } from './evento.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // RouterModule.forChild(VideoRoutes)
    ],
    declarations: [
        // VideoListComponent,
        // VideoFormComponent
    ],
    providers: [
        EventoService
    ]
})

export class EventoModule { }
