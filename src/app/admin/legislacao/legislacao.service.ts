import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';

@Injectable()
export class LegislacaoService {

    constructor(private db: FirestoreService) {
    }

    findAll() {
        return this.db.colWithIds$('legislacoes', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
        );
    }

    findLeisFederais() {
        return this.findByType('federal');
    }

    findDecretos() {
        return this.findByType('decreto');
    }

    findEmendas() {
        return this.findByType('emenda');
    }

    findConstituição() {
        return this.findByType('constituicao');
    }

    findByType(type: string) {
        return this.db.colWithIds$('legislacoes', ref => ref
            .where('enable', '==', true)
            .where('type', '==', type)
            .orderBy('created_at', 'desc')
        );
    }

    findById(id) {
        return this.db.docWithIds$('legislacoes/' + id);
    }

    add(legislacao) {
        legislacao.enable = true;
        this.db.add('legislacoes', legislacao)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(legislacao) {
        this.db.update('legislacoes/' + legislacao.id, legislacao)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(legislacao) {
        legislacao.enable = false;
        this.edit(legislacao);
    }

}
