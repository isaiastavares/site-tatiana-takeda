import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LegislacaoService } from './legislacao.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        // RouterModule.forChild(VideoRoutes)
    ],
    declarations: [
        // VideoListComponent,
        // VideoFormComponent
    ],
    providers: [
        LegislacaoService
    ]
})

export class LegislacaoModule { }
