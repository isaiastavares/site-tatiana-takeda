import { Routes } from '@angular/router';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoFormComponent } from './video-form/video-form.component';

export const VideoRoutes: Routes = [{
    path: '',
    redirectTo: '',
    pathMatch: 'full',
}, {
    path: '',
    children: [{
        path: '',
        component: VideoListComponent
    }, {
        path: 'video-form/:id',
        component: VideoFormComponent
    }, {
        path: 'video-form',
        component: VideoFormComponent
    }]
}];
