import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import { ToastrService } from 'ngx-toastr';
import { PageTitleService } from 'src/app/core/page-title/page-title.service';
import { fadeInAnimation } from 'src/app/core/route-animation/route.animation';
import { Video } from '../dto/video.dto';
import { VideoService } from '../video.service';

@Component({
    'selector': 'app-video-form',
    templateUrl: './video-form-component.html',
    styleUrls: ['./video-form-component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [fadeInAnimation]
})
export class VideoFormComponent implements OnInit {

    @HostBinding('@fadeInAnimation') fadeInAnimation = true;

    form: FormGroup;

    isEdit: Boolean = false;
    video: Video;

    constructor(private pageTitleService: PageTitleService, private route: ActivatedRoute,
        private router: Router, private toastrService: ToastrService, private fb: FormBuilder,
        private videoService: VideoService) {

        this.video = this.videoService.getBlankRecord();
        const videoId = this.route.snapshot.params.id;

        if (videoId) {
            this.isEdit = true;
            this.videoService.findById(videoId).subscribe(video => {
                this.video = video;
            });
        }
    }

    ngOnInit() {
        this.pageTitleService.setTitle(this.isEdit ? 'Editar Vídeo' : 'Adicionar Vídeo');
        this.form = this.fb.group({
            title: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
        });
    }

    back() {
        this.router.navigate(['admin/video/video-list']);
    }

    save(video) {
        if (this.form.valid) {
            if (this.isEdit) {
                this.videoService.edit(video);
                this.toastrService.success(null, 'Vídeo atualizado com sucesso!');
            } else {
                this.videoService.add(video);
                this.toastrService.success(null, 'Vídeo adicionado com sucesso!');
            }
            this.form.reset();
        }
        this.back();
    }

    chooseFile() {
        document.getElementById('video-thumb').click();
    }

    upload() {
        const storageRef = firebase.storage().ref();

        for (const selectedFile of [(<HTMLInputElement>document.getElementById('video-thumb')).files[0]]) {
            const path = `/images/video/${selectedFile.name}`;
            const iRef = storageRef.child(path);
            iRef.put(selectedFile).then((snapshot) => {
                this.video.thumb = snapshot.downloadURL;
            });
        }
    }
}
