import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { bootstrap4Mode, Modal } from 'ngx-modialog/plugins/bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PageTitleService } from 'src/app/core/page-title/page-title.service';
import { fadeInAnimation } from 'src/app/core/route-animation/route.animation';
import { Video } from '../dto/video.dto';
import { VideoService } from '../video.service';

bootstrap4Mode();

@Component({
    'selector': 'app-video-list',
    templateUrl: './video-list-component.html',
    styleUrls: ['./video-list-component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [fadeInAnimation]
})
export class VideoListComponent implements OnInit {

    @HostBinding('@fadeInAnimation') fadeInAnimation = true;

    videos: Video[];

    constructor(private pageTitleService: PageTitleService, private router: Router,
        public modal: Modal, private toastrService: ToastrService, private videoService: VideoService) {
    }

    ngOnInit() {
        this.pageTitleService.setTitle('Tatiana Takeda - Vídeos');

        this.videoService.findAll().subscribe(videos => {
            this.videos = videos;
        });
    }

    add() {
        this.router.navigate(['admin/video/video-form']);
    }

    edit(video) {
        this.router.navigate(['admin/video/video-form', video.id]);
    }

    delete(video) {
        const dialogRef = this.modal.confirm()
            .size('lg')
            .title('Excluir Vídeo')
            .cancelBtn('Cancelar')
            .okBtnClass('btn btn-danger')
            .message('Tem certeza que deseja excluir o vídeo: ' + video.name + '?')
            .open();

        dialogRef.result.then(sim => {
            if (sim) {
                this.videoService.disable(video);
                this.toastrService.success(null, 'Vídeo excluído com sucesso!');
            }
        });
    }

}
