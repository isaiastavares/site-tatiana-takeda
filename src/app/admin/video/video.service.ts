import { Injectable } from '@angular/core';
import { FirestoreService } from 'src/app/core/service/firestore.service';
import { Video } from './dto/video.dto';

@Injectable()
export class VideoService {

    constructor(private db: FirestoreService) {
    }

    getBlankRecord(): Video {
        return {
            id: '',
            thumb: '/assets/img/no_image.png',
            title: '',
            urlVideo: '',
            enable: true
        };
    }

    findAll() {
        return this.db.colWithIds$('videos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
        );
    }

    findByLimit(limit: number) {
        return this.db.colWithIds$('videos', ref => ref
            .where('enable', '==', true)
            .orderBy('created_at', 'desc')
            .limit(limit)
        );
    }

    findById(id) {
        return this.db.docWithIds$('videos/' + id);
    }

    add(video) {
        video.enable = true;
        this.db.add('video', video)
            .then(function (docRef) {
                console.log('Document written with ID: ', docRef.id);
            })
            .catch(function (error) {
                console.error('Error adding document: ', error);
            });
    }

    edit(video) {
        this.db.update('videos/' + video.id, video)
            .then(function () {
                console.log('Document successfully updated!');
            }).catch(function (error) {
                // The document probably doesn't exist.
                console.error('Error updating document: ', error);
            });
    }

    disable(video) {
        video.enable = false;
        this.edit(video);
    }

}
