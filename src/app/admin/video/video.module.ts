import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoRoutes } from './video.routing';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoFormComponent } from './video-form/video-form.component';
import { VideoService } from './video.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(VideoRoutes)
    ],
    declarations: [
        VideoListComponent,
        VideoFormComponent
    ],
    providers: [
        VideoService
    ]
})

export class VideoModule { }
