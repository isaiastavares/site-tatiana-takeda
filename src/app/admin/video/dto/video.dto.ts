export interface Video {
    id: string;
    thumb: string;
    title: string;
    urlVideo?: string;
    enable: boolean;
}
