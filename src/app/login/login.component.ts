import { Component, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../core/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login-component.html',
  styleUrls: ['./login-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent {

  email: string;
  password: string;
  mensagem: string;

  constructor(private authService: AuthService) {}

  login() {
    if (!this.email || !this.password) {
      this.mensagem = 'Informe o email e a senha, por favor.';
      return;
    }

    this.authService.login(this.email, this.password);
  }

}
