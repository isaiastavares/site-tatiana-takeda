import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { EventoService } from '../admin/evento/evento.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit, OnDestroy {

  eventos: any;
  eventosSub: any;

  constructor(private titleService: Title, private eventoService: EventoService) { }

  ngOnInit() {
    this.titleService.setTitle('Tatiana Takeda - Eventos');

    this.eventosSub = this.eventoService.findAll().subscribe(eventos => {
      this.eventos = eventos;
    });
  }

  ngOnDestroy() {
    this.eventosSub.unsubscribe();
  }

}
